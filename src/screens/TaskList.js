import React, { memo, useState } from 'react';
import { View, FlatList, Text, StyleSheet } from 'react-native';
import TaskContext, { Task } from '../models/Task';

const { useQuery } = TaskContext;

const TaskItem = memo(({ description }) => {
  return (
    <>
      <Text style={styles.text}>{description}</Text>
    </>
  );
});

export const TaskList = () => {
  const tasks = useQuery(Task);
  const [showTasks, setShowTasks] = useState(tasks.slice(0, 50));

  const queryTask = () => {
    setShowTasks((pre) => [
      ...pre,
      ...tasks.slice(pre.length, pre.length + 20),
    ]);
  };

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.list}
        data={showTasks}
        keyExtractor={(task) => task?._id?.toString()}
        renderItem={({ item }) => (
          <TaskItem
            description={item.description}
            isComplete={item.isComplete}
          />
        )}
        onEndReached={queryTask}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  list: {
    width: '100%',
  },
  text: {
    fontSize: 32,
  },
});
