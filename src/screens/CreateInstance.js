import React, { useCallback, useState } from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Button,
  TextInput,
  Text,
} from 'react-native';

import TaskContext, { Task } from '../models/Task';

const { useRealm, useQuery } = TaskContext;

const CreateInstance = ({ navigation }) => {
  const realm = useRealm();
  const tasks = useQuery(Task);
  const [createNum, setCreateNum] = useState('0');
  const [createTimes, setCreateTimes] = useState([]);

  // const readTest = () => {
  //   let test = tasks.toJSON();
  // }
  // console.log(readTest());

  const onCreateNumChanged = useCallback((text) => {
    setCreateNum(text.replace(/[^0-9]/g, ''));
  }, []);

  const createInstance = useCallback(() => {
    const start = new Date().getTime();
    realm.write(() => {
      for (let index = 0; index < createNum; index++) {
        realm.create(Task, Task.generate(`test ${tasks.length}`));
      }
    });

    const end = new Date().getTime();
    setCreateTimes((pre) => [...pre, end - start]);
    console.log(`create time ${end - start} milliseconds`);
  }, [createNum, realm, tasks]);

  const clearInstance = useCallback(() => {
    realm.write(() => {
      realm.deleteAll();
    });
    setCreateTimes([]);
  }, [realm]);

  const getAverageCreateTime = useCallback(() => {
    if (createTimes.length === 0) {
      return 0;
    }
    return Math.floor(
      createTimes.reduce((partialSum, a) => partialSum + a, 0) /
        createTimes.length,
    );
  }, [createTimes]);

  const goTaskListScreen = () => {
    navigation.push('TaskList');
  };

  return (
    <SafeAreaView>
      <StatusBar />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.container}>
          <TextInput
            style={styles.textInput}
            placeholder="Add instance number"
            keyboardType="numeric"
            onChangeText={onCreateNumChanged}
            value={createNum}
          />
          <Button
            title={`Add new ${createNum} instance`}
            onPress={createInstance}
          />
          <Button title={'Cleear Instance'} onPress={clearInstance} />
          <Button title={'Task List'} onPress={goTaskListScreen} />
          <Text style={styles.text}>{`Instances Size: ${tasks.length}`}</Text>
          <Text
            style={styles.text}
          >{`Average Create Time: ${getAverageCreateTime()} ms`}</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInput: {
    width: '90%',
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  text: {
    marginTop: 20,
    fontSize: 18,
  },
});

export default CreateInstance;
