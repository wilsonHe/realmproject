export const TYPES = Object.freeze({
  BOOL: 'bool',
  INT: 'int',
  FLOAT: 'float',
  DOUBLE: 'double',
  STRING: 'string',
  OBJECTID: 'objectId',
  DATA: 'data',
  DATE: 'date',
  LIST: 'list',
});
