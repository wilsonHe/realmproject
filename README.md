
## Quick start

Android version haven't tested yet.

```
npm run setup
npm run ios
```

## Realm Package Size 
---
Memory usage (didn't do anything):

Before install package: 210 MB

after install package: 210 MB

---

App size:

Before install package: 8.9 MB

after install package: 16.6 MB

## Realm Performance (JSC v10.22 RN 0.64)

Compute 10 times and check the memory usage and create time. if create size is 10 then it has 100 instances to be used. Each instance object has 24 bytes memory size.

Memory (query) means the memory usage when using the `useQuery` to get the `Realm.Result` object.

Memory (load all) means the memory usage when converting all the `Realm.Result` to `JSON` object.

| Create size | Average Create Time | Memory (write) | Memory (read) | Memory (load all) |
| ----------- |:------------------- |:-------------- |:------------- |:----------------- |
| 10          | 5 ms                | ~10 MB         | ~2MB          | 8MB               |
| 1000        | 20 ms               | 70 MB          | ~10MB         | 60MB              |
| 100000      | 1289 ms             | 657 MB         | ~20MB         | 1228MB            |


## Realm Performance (Hermes v11.0.0-rc.0 RN 0.66)


| Create size | Average Create Time | Memory (write) | Memory (query) | Memory (load all) |
| ----------- |:------------------- |:-------------- |:-------------- |:----------------- |
| 10          | 12 ms               | ~40 MB         | ~8MB           | ~8MB              |
| 1000        | 86 ms               | 60 MB          | ~10MB          | 75MB              |
| 100000      | 4801 ms             | 140 MB         | ~20MB          | 1843MB            |

# Realm Performance (Hermes v11.0.0-rc.1 RN 0.69)

| Create size | Average Create Time | Memory (write) | Memory (query) | Memory (load all) |
| ----------- |:------------------- |:-------------- |:-------------- |:----------------- |
| 10          | 5 ms                | ~9 MB          | ~2MB           | ~4MB              |
| 1000        | 65 ms               | 11 MB          | ~3MB           | 10MB              |
| 100000      | 3552 ms             | 47 MB          | 15MB           | 713MB             |