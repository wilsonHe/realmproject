/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import CreateInstance from './src/screens/CreateInstance';
import { TaskList } from './src/screens/TaskList';

import TaskContext from './src/models/Task';

const App = () => {
  const Root = createNativeStackNavigator();

  const { RealmProvider } = TaskContext;

  return (
    <RealmProvider>
      <NavigationContainer>
        <Root.Navigator>
          <Root.Screen name={'CreateInstance'} component={CreateInstance} />
          <Root.Screen name={'TaskList'} component={TaskList} />
        </Root.Navigator>
      </NavigationContainer>
    </RealmProvider>
  );
};

export default App;
