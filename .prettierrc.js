module.exports = {
  tabWidth: 2,
  bracketSpacing: true,
  jsxBracketSameLine: false,
  singleQuote: true,
  trailingComma: 'all',
  arrowParens: 'always',
};
